const border_sty1 = 'w-48 border-dotted border-4 border-blue-600 p-2';

const AuthorList = ({ authors }) => (
	//<div className="w-48 border-solid border-2 border-red-300">
	<div className={border_sty1}>
		{authors.map((a, i) => (
			<div key={i}>
				<div className="text-base pl-2">{a.name}</div>
			</div>
		))}
	</div>
);

export default AuthorList;

import gql from 'graphql-tag';
import { Query } from 'react-apollo';
import withData from '../config';

import AuthorList from './AuthorList';

import cn from 'classnames';

const query = gql`
	query {
		author {
			id
			name
		}
	}
`;

const status = false;

const Index = ({ authors }) => {
	return (
		<Query // <- Wrapping the main component with Query component from react-apollo
			query={query}
			fetchPolicy={'cache-and-network'}>
			{({ loading, data, error }) => {
				if (error) {
					return <div>Error..</div>;
				}
				return (
					<div className="p-10 border-solid border-4 border-gray-600 m-8">
						{status ? (
							<div className="text-4xl text-red-500 pb-3">My Authors </div>
						) : (
							<div className="text-4xl text-green-500 pb-3">My Authors </div>
						)}

						<AuthorList authors={data ? data.author : []} />
					</div>
				);
			}}
		</Query>
	);
};

export default withData(Index);
